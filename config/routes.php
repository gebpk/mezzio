<?php

declare(strict_types=1);

use App\Handler\HomePageHandler;
use App\Handler\PingHandler;
use ESourcing\Handler\MessageHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

return static function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {
    $app->get('/', HomePageHandler::class, 'home');
    $app->get('/api/ping', PingHandler::class, 'api.ping');
    $app->get('/prooph', MessageHandler::class);
    $app->get(
        '/fn',
        fn(ServerRequestInterface $r): ResponseInterface => new HtmlResponse("Hello world")
    );
};
