<?php

declare(strict_types=1);

namespace ESourcing\Message;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

/**
 * Description of SayHello
 *
 * @author cocomo
 */
class SayHello extends Command implements PayloadConstructable {

    use PayloadTrait;

    public function to(): string {
        return $this->payload['to'];
    }

}
