<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Event;

use ESourcing\Basket\Model\Basket\BasketId;
use ESourcing\Basket\Model\Basket\ShoppingSession;
use Prooph\EventSourcing\AggregateChanged;

/**
 * Description of ShoppingSessionStarted
 *
 * @author cocomo
 */
class ShoppingSessionStarted extends AggregateChanged {

    public function basketId(): BasketId {
        return BasketId::fromString($this->aggregateId());
    }

    public function shoppingSession(): ShoppingSession {
        return ShoppingSession::fromString($this->payload['shopping_session']);
    }

}
