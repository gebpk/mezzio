<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Event;

use ESourcing\Basket\Model\Basket\BasketId;
use ESourcing\Basket\Model\ERP\ProductId;
use Prooph\EventSourcing\AggregateChanged;

/**
 * Description of ProductAddedToBasket
 *
 * @author cocomo
 */
class ProductAddedToBasket extends AggregateChanged {

    public function basketId(): BasketId {
        return BasketId::fromString($this->aggregateId());
    }

    public function productId(): ProductId {
        return ProductId::fromString($this->payload['product_id']);
    }

}
