<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Basket;

use Ramsey\Uuid\Uuid;

/**
 * Description of BasketId
 *
 * @author cocomo
 */
final class BasketId {

    private string $basketId;

    public function __construct(string $basketId) {

        if (!Uuid::isValid($basketId)) {
            throw new InvalidArgumentException('Invalid Uuid for basket id');
        }

        $this->basketId = $basketId;
    }

    public function toString(): string {
        return $this->basketId;
    }

    public function __toString(): string {
        return $this->basketId;
    }

    public function equals($obj): bool {
        if (!$obj instanceof self) {
            return false;
        }

        return $obj->basketId === $this->basketId;
    }

    public static function fromString(string $basketId) {
        return new self($basketId);
    }

}
