<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Basket;

/**
 * Description of BasketId
 *
 * @author cocomo
 */
final class ShoppingSession {

    private string $shoppingSession;

    public function __construct(string $shoppingSession) {

        if (empty($shoppingSession)) {
            throw new InvalidArgumentException('Shopping sessiong must not be an empty string');
        }

        $this->shoppingSession = $shoppingSession;
    }

    public function toString(): string {
        return $this->shoppingSession;
    }

    public function __toString(): string {
        return $this->shoppingSession;
    }

    public function equals($obj): bool {
        if (!$obj instanceof self) {
            return false;
        }

        return $obj->shoppingSession === $this->shoppingSession;
    }

    public static function fromString(string $shoppingSession) {
        return new self($shoppingSession);
    }

}
