<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Exception;

use ESourcing\Basket\Model\ERP\ProductId;
use InvalidArgumentException;

/**
 * Description of UnknownProduct
 *
 * @author cocomo
 */
class UnknownProduct extends InvalidArgumentException {

    public static function withProductId(ProductId $productId): self {
        return new self(
            "Product with id '{$productId->toString()}' is unknown"
        );
    }

}
