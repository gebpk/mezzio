<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Exception;

use ESourcing\Basket\Model\Basket\BasketId;
use ESourcing\Basket\Model\ERP\ProductId;
use RuntimeException;

/**
 * Description of ProductAddedTwice
 *
 * @author cocomo
 */
class ProductAddedTwice extends RuntimeException {

    public static function toBasket(BasketId $basketId, ProductId $productId): self {
        return new self(sprintf(
                'Product %s added twice to basket %s',
                $productId->toString(),
                $basketId->toString(),
        ));
    }

}
