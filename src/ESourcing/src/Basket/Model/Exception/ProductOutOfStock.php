<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\Exception;

use ESourcing\Basket\Model\ERP\ProductId;
use RuntimeException;

/**
 * Description of ProductOutOfStock
 *
 * @author cocomo
 */
class ProductOutOfStock extends RuntimeException {

    public static function withProductId(ProductId $productId): self {
        return new self(
            "Product with id {$productId->toString()} is out of stock",
        );
    }

}
