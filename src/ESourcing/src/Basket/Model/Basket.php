<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model;

use ESourcing\Basket\Model\Basket\BasketId;
use ESourcing\Basket\Model\Basket\ShoppingSession;
use ESourcing\Basket\Model\ERP\ERP;
use ESourcing\Basket\Model\ERP\ProductId;
use ESourcing\Basket\Model\Event\ProductAddedToBasket;
use ESourcing\Basket\Model\Event\ShoppingSessionStarted;
use ESourcing\Basket\Model\Exception\ProductAddedTwice;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

/**
 * Aggregate Root represents a business process (sequential events) bound
 * by business rules.
 * 
 * Rules:
 * A shopping session starts with an empty basket.
 * Each shopping session is assigned its own basket.
 * A product can only be added to a basket if at least one product is available in stock.
 * Product quantity in a basket must not be higher than available stock.
 * If stock is reduced by a checkout, product quantity in currently active baskets needs to be checked and conflicts resolved.
 * If product quantity in a basket is reduced to zero or less the product is removed from the basket.
 * A checkout can only be made if no unresolved quantity-stock-conflicts exist for the basket.
 *
 * @author cocomo
 */
class Basket extends AggregateRoot {

    private BasketId $basketId;
    private ShoppingSession $shoppingSession;

    /**
     * 
     * @var ProductId[]
     */
    private array $products = [];

    /**
     * Initiates shopping session. In flux terminology this is an action creator.
     * 
     * @param ShoppingSession $session
     * @param BasketId $basketId
     */
    public static function startShoppingSession(ShoppingSession $session, BasketId $basketId): Basket {
        // Start a new aggregate life cycle. Lifecycle are the discrete states of a business process
        // from its birth to its death.
        $self = new self();
        $self->recordThat(ShoppingSessionStarted::occur(
                $basketId->toString(),
                [
                    'shopping_session' => $session->toString(),
                ]
        ));
        return $self;
    }

    public function addProduct(ProductId $productId, ERP $erp): void {
        if (array_key_exists($productId->toString(), $this->products)) {
            throw ProductAddedTwice::toBasket($this->basketId, $productId);
        }

        $productStock = $erp->getProductStock($productId);

        if (!$productStock) {
            $this->recordThat(ProductAddedToBasket::occur(
                    $this->basketId->toString(),
                    [
                        'product_id'     => $productId->toString(),
                        'stock_version'  => null,
                        'stock_quantity' => null,
                        'quantity'       => 1,
                    ]
            ));
            return;
        }

        if ($productStock->quantity() === 0) {
            throw Exception\ProductOutOfStock::withProductId($productId);
        }

        $this->recordThat(ProductAddedToBasket::occur($this->basketId->toString(), [
                'product_id' => $productId->toString(),
        ]));
    }

    protected function aggregateId(): string {
        return $this->basketId->toString();
    }

    /**
     * Absorbs the domain event of a business process. In flux terminology this is 
     * a reducer (without immutability)
     * 
     * @param AggregateChanged $event
     * @return void
     */
    protected function apply(AggregateChanged $event): void {

        // Shopping session started
        if ($event instanceof ShoppingSessionStarted) {
            $this->basketId        = $event->basketId();
            $this->shoppingSession = $event->shoppingSession();
            return;
        }

        // Product added to basket
        if ($event instanceof ProductAddedToBasket) {
            $this->products[$event->productId()->toString()] = $event->productId();
            return;
        }
    }

}
