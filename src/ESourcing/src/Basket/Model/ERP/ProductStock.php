<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\ERP;

/**
 * Description of ProductStock
 *
 * @author cocomo
 */
final class ProductStock {

    private int $version;
    private int $quantity;
    private ProductId $productId;

    public function __construct(ProductId $prodId, int $qty, int $ver) {
        $this->productId = $prodId;
        $this->quantity  = $qty;
        $this->version   = $ver;
    }

    public static function fromArray(array $data): self {
        return new self(
            ProductId::fromString($data['product_id'] ?? ''),
            $data['quantity'] ?? 0,
            $data['version'] ?? 0,
        );
    }

    public function toArray(): array {
        return [
            'product_id' => $this->productId->toString(),
            'quantity'   => $this->quantity,
            'version'    => $this->version,
        ];
    }

    public function productId(): ProductId {
        return $this->productId;
    }

    public function version(): int {
        return $this->version;
    }

    public function quantity(): int {
        return $this->quantity;
    }

    public function __toString() {
        return json_encode($this->toArray());
    }

}
