<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\ERP;

/**
 * Description of ProductId
 *
 * @author cocomo
 */
class ProductId {

    protected string $id;

    public function __construct(string $id) {
        if (empty($id)) {
            throw new InvalidArgumentException('Product id cannot be empty string');
        }

        $this->id = $id;
    }

    public static function fromString(string $id): ProductId {
        return new self($id);
    }

    public function toString(): string {
        return $this->id;
    }

    public function equals($obj): bool {
        if (!$obj instanceof self) {
            return false;
        }

        return $obj->id === $this->id;
    }

    public function __toString(): string {
        return $this->id;
    }

}
