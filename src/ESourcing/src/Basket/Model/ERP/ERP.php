<?php

declare(strict_types=1);

namespace ESourcing\Basket\Model\ERP;

/**
 *
 * @author cocomo
 */
interface ERP {

    public function getProductStock(ProductId $productId): ?ProductStock;
}
