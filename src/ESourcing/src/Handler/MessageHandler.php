<?php

declare (strict_types=1);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ESourcing\Handler;

use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Description of MessageHandler
 *
 * @author cocomo
 */
class MessageHandler implements RequestHandlerInterface {

    public function handle(ServerRequestInterface $request): ResponseInterface {
        $r = new TextResponse('Hello world');
        return $r;
    }

}
