<?php

declare(strict_types=1);

namespace ESourcingTest\Basket\Model;

use ESourcing\Basket\Model\Basket;
use ESourcing\Basket\Model\Basket\BasketId;
use ESourcing\Basket\Model\Basket\ShoppingSession;
use ESourcing\Basket\Model\ERP\ERP;
use ESourcing\Basket\Model\ERP\ProductId;
use ESourcing\Basket\Model\ERP\ProductStock;
use ESourcing\Basket\Model\Event\ProductAddedToBasket;
use ESourcing\Basket\Model\Event\ShoppingSessionStarted;
use ESourcing\Basket\Model\Exception\ProductAddedTwice;
use ESourcingTest\TestCase;
use Prooph\EventSourcing\AggregateChanged;
use Ramsey\Uuid\Uuid;

/**
 * Description of BasketTest
 *
 * @author cocomo
 */
class BasketTest extends TestCase {

    private ShoppingSession $shoppingSession;
    private BasketId $basketId;
    private ProductId $product1;

    protected function setUp(): void {
        $this->shoppingSession = ShoppingSession::fromString('123');
        $this->basketId        = BasketId::fromString(Uuid::uuid4()->toString());
        $this->product1        = ProductId::fromString('A1');
    }

    protected function reconstituteBasketFromHistory(AggregateChanged ...$events): Basket {
        return $this->reconstituteAggregateFromHistory(
                Basket::class,
                $events);
    }

    protected function shoppingSessionStarted(): ShoppingSessionStarted {
        return ShoppingSessionStarted::occur($this->basketId->toString(), [
                'shopping_session' => $this->shoppingSession->toString()
        ]);
    }

    protected function product1Added(): ProductAddedToBasket {
        return ProductAddedToBasket::occur($this->basketId->toString(), [
                'product_id' => $this->product1->toString(),
        ]);
    }

    protected function product1ERP(int $stockQty = 5): ERP {
        $erp = $this->prophesize(ERP::class);

        $erp->getProductStock($this->product1)->willReturn(
            ProductStock::fromArray([
                'product_id' => $this->product1->toString(),
                'quantity'   => $stockQty,
                'version'    => 1
            ])
        );

        return $erp->reveal();
    }

    /**
     * @test
     */
    function it_starts_a_shopping_session() {
        $basket = Basket::startShoppingSession(
                $this->shoppingSession,
                $this->basketId);

        /* @var $events AggregateChanged[] */
        $events = $this->popRecordedEvents($basket);

        $this->assertCount(1, $events);

        /* @var $event ShoppingSessionStarted */
        $event = $events[0];

        $this->assertSame(
            ShoppingSessionStarted::class,
            $event->messageName(),
        );
        $this->assertTrue(
            $this->basketId->equals($event->basketId()),
        );
        $this->assertTrue(
            $this->shoppingSession->equals($event->shoppingSession()),
        );
    }

    /**
     * @test
     */
    function it_adds_a_product_if_stock_qty_is_greater_than_zero() {
        $basket = $this->reconstituteBasketFromHistory(
            $this->shoppingSessionStarted(),
        );

        $basket->addProduct($this->product1, $this->product1ERP());

        /* @var $events AggregateChanged[] */
        $events = $this->popRecordedEvents($basket);

        $this->assertCount(1, $events);

        /* @var $event ProductAddedToBasket */
        $event = $events[0];

        $this->assertSame(
            ProductAddedToBasket::class,
            $event->messageName(),
        );
        $this->assertTrue(
            $this->basketId->equals($event->basketId()),
        );
        $this->assertTrue(
            $this->product1->equals($event->productId()),
        );
    }

    /**
     * @test
     */
    function it_throws_exception_if_product_is_added_twice() {
        $basket = $this->reconstituteBasketFromHistory(
            $this->shoppingSessionStarted(),
            $this->product1Added(),
        );

        $this->expectException(ProductAddedTwice::class);
        $basket->addProduct($this->product1, $this->product1ERP());
    }

    /**
     * @test
     */
    function it_adds() {

        $f = fn($a, $b) => (
            $a + $b
        );
        $this->assertTrue($f(1, 2) === 3);

        $sum = array_reduce(
            [1, 2, 3, 4, 5],
            fn($prev, $next) => $prev + $next,
            0
        );
        $this->assertTrue($sum === 15);
    }

}
